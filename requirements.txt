dj-database-url==0.4.2
dj-static==0.0.6
Django==2.0
django-materialize-css==0.0.1
django-money==0.12.3
django-toolbelt==0.0.1
gunicorn==19.7.1
mysqlclient==1.3.12
olefile==0.44
Pillow==4.3.0
psycopg2==2.7.3.2
py-moneyed==0.7.0
pytz==2017.3
static3==0.7.0
whitenoise==3.3.1
-e git+git://github.com/python-force/django-admin-multiupload.git#egg=mutliupload
