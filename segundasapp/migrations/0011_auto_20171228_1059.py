# Generated by Django 2.0 on 2017-12-28 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('segundasapp', '0010_segunda_categoria'),
    ]

    operations = [
        migrations.AlterField(
            model_name='segunda',
            name='categoria',
            field=models.CharField(choices=[('A', 'Acero'), ('M', 'Material de Construcción'), ('V', 'Varilla'), ('O', 'Otros')], max_length=1),
        ),
    ]
