# Generated by Django 2.0 on 2017-12-20 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('segundasapp', '0005_auto_20171219_1559'),
    ]

    operations = [
        migrations.AlterField(
            model_name='segunda',
            name='imagen',
            field=models.ImageField(upload_to='.segundasapp/static/segundasapp/imagenesegundas'),
        ),
    ]
