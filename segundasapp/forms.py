from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from multiuploadform.fields import MultiImageField
from django.utils import html

class ContactoMail(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre','type':'text','class':'validate'}),max_length=100)
    apellido_contacto = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Apellidos','type':'text','class':'validate'}),max_length=100)
    telefono_contacto = forms.CharField(widget=forms.TextInput(attrs={'placeholder': '55-55-55-55','type':'tel','class':'validate'}),max_length=14,min_length=8)
    email_contacto = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'ejemplo@ejemplo.com','type':'email','class':'validate'}),max_length = 100)
    titulo_contacto = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Título','type':'text','class':'validate'}),max_length=100,required=False)
    textarea_contacto = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Breve descripción'}),max_length = 500)
    precio_contacto = forms.FloatField(widget=forms.NumberInput(attrs={'placeholder': 'Desde','type': 'number','min':'0','max':'100000000','step':'.01','value':'0','name':'min'}),required=False)
    #imagenes_contacto = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),required=False)
    imagenes_contacto = MultiImageField(min_num=1, max_num=3, max_file_size=1024*1024*5,required=False)

CAT_CHOICES= [
    ('T', 'Todos'),
    ('A', 'Acero'),
    ('M', 'Material de Construcción'),
    ('O', 'Otros'),
    ('V', 'Varilla'),
    ]

class BusquedaSegundas(forms.Form):
    titulo_busqueda = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Título o palabra clave','type':'text','class':'validate'}),max_length=100,required=False)
    precio_busqueda_min = forms.FloatField(widget=forms.NumberInput(attrs={'placeholder': 'Desde','type': 'number','min':'0','max':'100000000','step':'.01','value':'0','name':'min'}),required=False)
    precio_busqueda_max = forms.FloatField(widget=forms.NumberInput(attrs={'placeholder': 'Hasta','type': 'number','min':'0','max':'100000000','step':'.01','value':'0','name':'max'}),required=False)
    categoria_busqueda= forms.CharField(label='Seleccione una categoría', widget=forms.Select(choices=CAT_CHOICES))

class ContactoSegunda(forms.Form):
    nombre_segunda = forms.CharField(widget=forms.TextInput(attrs={'type':'text','class':'validate'}),max_length=100)
    apellido_segunda = forms.CharField(widget=forms.TextInput(attrs={'type':'text','class':'validate'}),max_length=100)
    telefono_segunda = forms.CharField(widget=forms.TextInput(attrs={'type':'tel','class':'validate'}),max_length=14,min_length=8)
    email_segunda = forms.EmailField(widget=forms.TextInput(attrs={'type':'email','class':'validate'}),max_length = 100)
    textarea_segunda = forms.CharField(widget=forms.Textarea(attrs={'class':'materialize-textarea'}),max_length = 500,required=False)
