from __future__ import unicode_literals
from djmoney.models.fields import MoneyField
from django.db import models
# Create your models here.
class Attachment(models.Model):
    fileattach = models.FileField(upload_to='segundasapp/static/segundasapp/imagenescontacto')
    fichacontacto = models.ForeignKey('Contacto', related_name='images', blank=True, null=True,on_delete=models.CASCADE)
    def __str__(self):
        return self.filename

    @property
    def filename(self):
        return self.fileattach.name.rsplit('/', 1)[-1]

class Contacto(models.Model):
    fecha = models.DateField()
    titulo = models.CharField( max_length=100)

class Segunda(models.Model):
    #idsegunda = models.AutoField()  # Field name made lowercase.
    CAT_SEG = (
        ('A', 'Acero'),
        ('M', 'Material de Construcción'),
        ('V', 'Varilla'),
        ('O', 'Otros'),
    )
    titulo = models.CharField( max_length=100)  # Field name made lowercase.
    contenido = models.TextField()  # Field name made lowercase.
    fecha = models.DateField()
    precio = MoneyField(max_digits=10, decimal_places=2, default_currency='MXN')
    categoria = models.CharField(max_length=1, choices=CAT_SEG)
    def __str__(self):
        return str(self.titulo)

class Image(models.Model):
    fileimage = models.FileField('File', upload_to='segundasapp/static/segundasapp/imagenesegundas')
    gallery = models.ForeignKey('Segunda', related_name='images', blank=True, null=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.filename

    @property
    def filename(self):
        return self.fileimage.name.rsplit('/', 1)[-1]
