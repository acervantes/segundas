flag = false;
$(document).ready(function(){
  $('form').submit(function(e) {
    if(flag){
      return;
    }
    else{
      e.preventDefault();
      webToLead($(this));
      flag = true;
      $(this).submit();
    }
    });
  });


function webToLead(thisObj) {
    firstName = thisObj.find("#first_name").val();
    lastName = thisObj.find("#last_name").val();
    email = thisObj.find("#email").val();
    phone = thisObj.find("#phone").val();
    
    var form = document.createElement("form");
    form.name = "frm";
    form.method = "POST";
    form.action = "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8";
    //form.setAttribute("target", customHiddenIframeName);
    // Your org ID
    var elementOID = document.createElement("input");
    elementOID.name="oid";
    elementOID.value="00DA0000000BCjm";
    elementOID.setAttribute("type", "hidden");
    form.appendChild(elementOID);

    // SFDC redirects to retURL in the response to the form post
    var elementRetURL = document.createElement("input");
    elementRetURL.name="retURL";
    elementRetURL.value = location.host;
    elementRetURL.setAttribute("type", "hidden");
    form.appendChild(elementRetURL);

    var elementcodigo = document.createElement("input");
    elementcodigo.id="00NF0000008UJpt";
    elementcodigo.name="00NF0000008UJpt";
    elementcodigo.value="Segundas";
    elementcodigo.setAttribute("type", "hidden");
    form.appendChild(elementcodigo);

    var elementlead = document.createElement("input");
    elementlead.id="lead_source";
    elementlead.name="lead_source";
    elementlead.value="Contacto Página Web";
    elementlead.setAttribute("type", "hidden");
    form.appendChild(elementlead);
    // Whatever params you want;
    // generate a form from Customize | Leads | Web-to-Lead to figure out more
    var elementFirstName = document.createElement("input");
    elementFirstName.name="first_name";
    elementFirstName.value=firstName;
    elementFirstName.setAttribute("type", "hidden");
    form.appendChild(elementFirstName);

    var elementLastName = document.createElement("input");
    elementLastName.name="last_name";
    elementLastName.value=lastName;
    elementLastName.setAttribute("type", "hidden");
    form.appendChild(elementLastName);

    var elementEmail = document.createElement("input");
    elementEmail.name="email";
    elementEmail.value=email;
    elementEmail.setAttribute("type", "hidden");
    form.appendChild(elementEmail);

    var elementPhone = document.createElement("input");
    elementPhone.name="phone";
    elementPhone.value=phone;
    elementPhone.setAttribute("type", "hidden");
    form.appendChild(elementPhone);

    document.body.appendChild(form);
    form.submit();
    var $toastContent = $('<span>Se han recibido tus datos!<br>En breve te\
    contactaremos...</span>');
    Materialize.toast($toastContent, 5000);

}
