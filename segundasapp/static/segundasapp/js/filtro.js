function ajaxconsegunda(thisObj){
  formData = thisObj.serializeArray();
  formData = JSON.stringify(formData);
  tokencsrf = thisObj.children("input[name='csrfmiddlewaretoken']").val()
  $.ajaxSetup({
    headers: { "X-CSRFToken": tokencsrf }
  });
  $.ajax({
    method: 'POST',
    data: {'formData':formData},
    url: '/get_contacto_segunda/',
    success: function(newData) {
      $('.modal').modal('close');
      var $toastContent = $('<span>Se han recibido tus datos!<br>En breve te\
       contactaremos...</span>');
       Materialize.toast($toastContent, 5000);
       return true;
    },
    error: function(newData) {
      var $toastContent = $('<span>Error en envio de solictud...<br>\
      Verificar datos</span>');
      Materialize.toast($toastContent, 5000);
      return false;
    }
  });
}

//Javascript para enviar leads a salesforce
function webToLead(thisObj) {
    firstName = thisObj.find("#id_nombre_segunda").val();
    lastName = thisObj.find("#id_apellido_segunda").val();
    email = thisObj.find("#id_email_segunda").val();
    phone = thisObj.find("#id_telefono_segunda").val();
    var customHiddenIframeName='JLA_API';
  	if(!document.getElementById(customHiddenIframeName)){
  		var theiFrame=document.createElement("iframe");
  		theiFrame.id=customHiddenIframeName;
  		theiFrame.name=customHiddenIframeName;
  		theiFrame.src='about:blank';
  		theiFrame.style.display='none';
  		document.body.appendChild(theiFrame);
  	}
    var form = document.createElement("form");
    form.name = "frm";
    form.method = "POST";
    form.action = "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8";
    form.setAttribute("target", customHiddenIframeName);
    // Your org ID
    var elementOID = document.createElement("input");
    elementOID.name="oid";
    elementOID.value="00DA0000000BCjm";
    elementOID.setAttribute("type", "hidden");
    form.appendChild(elementOID);

    // SFDC redirects to retURL in the response to the form post
    var elementRetURL = document.createElement("input");
    elementRetURL.name="retURL";
    elementRetURL.value="http://127.0.0.1:8000/";
    elementRetURL.setAttribute("type", "hidden");
    form.appendChild(elementRetURL);

    var elementcodigo = document.createElement("input");
    elementcodigo.id="00NF0000008UJpt";
    elementcodigo.name="00NF0000008UJpt";
    elementcodigo.value="Segundas";
    elementcodigo.setAttribute("type", "hidden");
    form.appendChild(elementcodigo);

    var elementlead = document.createElement("input");
    elementlead.id="lead_source";
    elementlead.name="lead_source";
    elementlead.value="Contacto Página Web";
    elementlead.setAttribute("type", "hidden");
    form.appendChild(elementlead);

    // generate a form from Customize | Leads | Web-to-Lead to figure out more
    var elementFirstName = document.createElement("input");
    elementFirstName.name="first_name";
    elementFirstName.value=firstName;
    elementFirstName.setAttribute("type", "hidden");
    form.appendChild(elementFirstName);

    var elementLastName = document.createElement("input");
    elementLastName.name="last_name";
    elementLastName.value=lastName;
    elementLastName.setAttribute("type", "hidden");
    form.appendChild(elementLastName);

    var elementEmail = document.createElement("input");
    elementEmail.name="email";
    elementEmail.value=email;
    elementEmail.setAttribute("type", "hidden");
    form.appendChild(elementEmail);

    var elementPhone = document.createElement("input");
    elementPhone.name="phone";
    elementPhone.value=phone;
    elementPhone.setAttribute("type", "hidden");
    form.appendChild(elementPhone);
    document.body.appendChild(form);
    form.submit()
    ajaxconsegunda(thisObj)
}

function ordenarsegunda(thisObj){
    selec = parseInt(thisObj.val());
    if(selec <= 2){
      ordenar = "fecha";
      if(selec==1){
        asc = 0;
      }
      else{
        asc = 1;
      }
    }
    else{
      ordenar = "precio";
      if(selec == 3){
        asc = 0;
      }
      else{
        asc = 1;
      }
    }
    //inicio ajax ordenar
    $.ajax({
      method: 'GET',
      data: {'formData':formData1,'ordenar':ordenar,'asc':asc},
      url: '/ordenar_segundas/',
      success: function(newData) {
        var JSONObject = JSON.parse(newData);
        $("#segundas").html(JSONObject.segundas);
        $('.modal').modal();
        $( document ).ready(function() {
          $('.testcar').each(function(idx, item) {
            var carouselId = "carousel" + idx;
            this.id = carouselId;
            $(this).slick({
              slide: "#" + carouselId + " .option",
              appendArrows: "#" + carouselId + " .prev_next"
            });
          });
          $("form[name='contactosegunda']").submit(function(e) {
            e.preventDefault();
            webToLead($(this));
          });
        });
      },
      error: function() {
        alert("hubo un error");
      }
    });
    // fin ajax ordenar
}
// Aqui empieza la principal
$( document ).ready(function() {
  ordenar = "";
  formData1 = $('#filtross').serializeArray();
  formData1 = JSON.stringify(formData1);
  asc = 2;
  $('.modal').modal();
  $('select').material_select();
  $('.materialboxed').materialbox();
  $('#id_categoria_busqueda').change(function(){
    $('#filtross').submit();
    });
  $('#ordenarselect').change(function(){
    ordenarsegunda($(this))
    });
  $('form').submit(function(e) {
    e.preventDefault();
    if($(this).attr("id")=='filtrossearch'){
      valor = $(this).find("input").val();
      $('#filtross').find("input[name='titulo_busqueda']").val(valor);
      $('#filtross').submit();
    }
    if($(this).attr("id")=='filtross' || $(this).attr("id")=='filtrossearch'){
      minimo = parseFloat($(this).find("input[name='precio_busqueda_min']").val());
      if( minimo !== minimo){
        minimo = 0;
        $(this).find("input[name='precio_busqueda_min']").val(0)
      }
      maximo = parseFloat($(this).find("input[name='precio_busqueda_max']").val())
      if(maximo !== maximo){
        maximo = 0;
        $(this).find("input[name='precio_busqueda_max']").val(0)
      }
      if( minimo > maximo){
        alert("el precio mínimo debe ser menor al máximo!!")
          return false;
       }
      else{
         formData1 = $("#filtross").serializeArray();
         formData1 = JSON.stringify(formData1);
         $.ajax({
           method: 'GET',
           data: {'formData':formData1},
           url: '/filtro_segundas/',
           success: function(newData) {
             var JSONObject = JSON.parse(newData);
             $("#segundas").html(JSONObject.segundas);
             $("#pagination").html(JSONObject.paginate);
             $('.modal').modal();
             $('body,html').animate({
               scrollTop: 0
             }, 600);
             $("#selectordenar select").val("0").change();
             $("#selectordenar input").val("Ordenar por: ").change();
             e.preventDefault();
             $( document ).ready(function() {
               $("#selectordenar select").val("0").change();
               $('#pagination a').click(function(){
                 if(parseInt($(this).text())){
                   page = parseInt($(this).text());
                 }
                 else{
                   currpage = parseInt($("#pagination").find(".active").attr('id'));
                   if($(this).attr('id')=="page-left"){
                     page = currpage - 1;
                   }
                   else {
                     page = currpage + 1;
                   }
                 }
                 $.ajax({
                   method: 'GET',
                   data: {'formData':formData1,'page':page,"ordenar":ordenar,"asc":asc},
                   url: '/page_segundas/',
                   success: function(newData) {
                     var JSONObject = JSON.parse(newData);
                     $("#segundas").html(JSONObject.segundas);
                     $('body,html').animate({
                       scrollTop: 0
                     }, 600);
                     $("#pagination").find(".active").addClass('waves-effect');
                     $("#pagination").find(".active").removeClass('active');
                     $("#pagination").find("#"+JSONObject.pageactive).addClass('active');
                     $("#pagination").find("#"+JSONObject.pageactive).removeClass('waves-effect');
                     $('.modal').modal();
                     $( document ).ready(function() {
                       $('.testcar').each(function(idx, item) {
                         var carouselId = "carousel" + idx;
                         this.id = carouselId;
                         $(this).slick({
                           slide: "#" + carouselId + " .option",
                           appendArrows: "#" + carouselId + " .prev_next"
                         });
                       });
                       $("form[name='contactosegunda']").submit(function(e) {
                         e.preventDefault();
                         webToLead($(this));
                       });
                       $("form[name='ordenarsegunda']").submit(function(e) {
                         e.preventDefault();
                         ordenarsegunda($(this));
                       });
                     });
                   },
                   error: function() {
                     alert("hubo un error");
                   }
                 });
               });
               $('.testcar').each(function(idx, item) {
                 var carouselId = "carousel" + idx;
                 this.id = carouselId;
                 $(this).slick({
                   slide: "#" + carouselId + " .option",
                   appendArrows: "#" + carouselId + " .prev_next"
                 });
               });
               $("form[name='contactosegunda']").submit(function(e) {
                 e.preventDefault();
                 ajaxconsegunda($(this));
               });
               $("form[name='ordenarsegunda']").submit(function(e) {
                 e.preventDefault();
                 ordenarsegunda($(this));
               });
             });
           },
           error: function() {
             alert("hubo un error");
           }
         });
         return true;
       }
      }
  else{
    if($(this).attr("id")=='ordenarfecha' || $(this).attr("id")=='ordenarprecio'){
      ordenarsegunda($(this));
    }
    else{
      webToLead($(this));
    }
  }
  });
});
