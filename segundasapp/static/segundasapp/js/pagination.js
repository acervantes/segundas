$( document ).ready(function() {
  ordenar="";
  $('#pagination a').click(function(){
    if(parseInt($(this).text())){
      page = parseInt($(this).text());
    }
    else{
      currpage = parseInt($("#pagination").find(".active").attr('id'));
      if($(this).attr('id')=="page-left"){
        page = currpage - 1;
      }
      else {
        page = currpage + 1;
      }
    }
    $.ajax({
      method: 'GET',
      data: {'formData':formData1,'page':page,"ordenar":ordenar,"asc":asc},
      url: '/page_segundas/',
      success: function(newData) {
        var JSONObject = JSON.parse(newData);
        $("#segundas").html(JSONObject.segundas);
        $('.modal').modal();
        $('body,html').animate({
          scrollTop: 0
        }, 600);
        $("#pagination").find(".active").addClass('waves-effect');
        $("#pagination").find(".active").removeClass('active');
        $("#pagination").find("#"+JSONObject.pageactive).addClass('active');
        $("#pagination").find("#"+JSONObject.pageactive).removeClass('waves-effect');
        //$("#pagination").find("#"+JSONObject.pageactive).addClass('active');
        $(document).ready(function() {
          $('.testcar').each(function(idx, item) {
            var carouselId = "carousel" + idx;
            this.id = carouselId;
            $(this).slick({
              slide: "#" + carouselId + " .option",
              appendArrows: "#" + carouselId + " .prev_next",
            });
          });
          $("form[name='contactosegunda']").submit(function(e) {
            e.preventDefault();
            ajaxconsegunda($(this));
          });
          $("form[name='ordenarsegunda']").submit(function(e) {
            e.preventDefault();
            ordenarsegunda($(this));
          });
        });
      },
      error: function() {
        alert("hubo un error");
      }
    });
  });
});
