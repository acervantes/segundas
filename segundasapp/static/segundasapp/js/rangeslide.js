$(document).ready(function(){
  var slider = document.getElementById('test-slider');
  noUiSlider.create(slider, {
  	start: [0, 250000],
    connect: true,
  	step: 2500,
  	range: {
  		'min': [ 0 ],
  		'max': [ 250000 ]
  	},
  	format: wNumb({
  		decimals: 2,
  		//thousand: '.',
  		//postfix: ' (MXN $)',
  	})
  });
  var inputNumbermin = document.getElementById('id_precio_busqueda_min');
  var inputNumbermax = document.getElementById('id_precio_busqueda_max');

  slider.noUiSlider.on('update', function( values, handle ) {
	var value = values[handle];
  if ( !handle ) {
		inputNumbermin.value = value;
	}
  else{
    inputNumbermax.value = value;
  }

});
  inputNumbermin.addEventListener('change', function(){
  	slider.noUiSlider.set([this.value, null]);
  });
  inputNumbermax.addEventListener('change', function(){
  	slider.noUiSlider.set([null, this.value]);
  });
});
