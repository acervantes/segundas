$( document ).ready(function() {
  $('.testcar').each(function(idx, item) {
    var carouselId = "carousel" + idx;
    this.id = carouselId;
    $(this).slick({
      slide: "#" + carouselId + " .option",
      appendArrows: "#" + carouselId + " .prev_next"
    });
  });
});
