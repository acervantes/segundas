from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
import json
import time
import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.middleware.csrf import get_token
from django.views.generic.edit import FormView
#Importar formas.
from .forms import *
from .models import *
import math
#limite paginacion
lim = 6
correocontacto="acervantes@grupobsv.com.mx"
# Create your views here.
def index(request):
    segundas = Segunda.objects.filter().order_by('-fecha')
    cuenta = math.ceil(segundas.count()/3)
    segundas = segundas[:3]
    formseg = ContactoSegunda(request.POST)
    return render(request, 'segundasapp/index.html',{'segundas':segundas,\
    'count':range(cuenta)})

def google5a135354c3e98127(request):
    return render(request, 'segundasapp/google5a135354c3e98127.html')
#Para contacto.
def contacto(request):
    form = ContactoMail(request.POST,request.FILES)
    return render(request, 'segundasapp/contacto.html',{'form':form})
    #Para forma de contacto.

def prueba(request):
    print("plueba")
    return render(request, 'segundasapp/prueba.html')

#obtener datos de interesado en post de segunda
def get_contacto_segunda(request):
    form = ContactoSegunda(request.POST)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # check whether it's valid:
        body = json.loads(request.POST.get('formData'))
        formdict = {}
        for i in body:
            formdict[i['name']]= i['value']
        # datos contacto segunda
        nombre = formdict['nombre_segunda']
        apellido = formdict['apellido_segunda']
        telefono = formdict['telefono_segunda']
        email = formdict['email_segunda']
        observaciones = formdict['textarea_segunda']
        # datos segunda
        titulo = formdict['titulo_segunda']
        contenido = formdict['contenido_segunda']
        llave = formdict['llave_segunda']
        precio = formdict['precio_segunda']

        ctx = {
            'nombre': nombre,
            'apellido': apellido,
            'telefono': telefono,
            'correo': email,
            'observaciones': observaciones,
            'contenido': contenido,
            'precio': precio,
            'llave': llave,
            'segunda': titulo
        }
        # enviar datos de correo
        subject = "Segundas"
        to = [correocontacto]
        from_email = 'acervantesbsv@gmail.com'
        message = render_to_string('segundasapp/email.html',ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        if(msg.send()):
            print("sent")
        # redirect to a new URL:
        return HttpResponse("éxito")
    else:
        print("No valida!")
        return HttpResponse('error')

def get_mensaje_contacto(request):
    # create a form instance and populate it with data from the request:
    form = ContactoMail(request.POST)
    nombre = request.POST.get("first_name")
    print(nombre)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # check whether it's valid:
    # if form.is_valid():
        nombre = request.POST.get("first_name")
        apellido = request.POST.get("last_name")
        telefono = request.POST.get("phone")
        email = request.POST.get("email")
        texto = request.POST.get("textarea_contacto")
        titulo = request.POST.get("titulo_contacto")
        precio = request.POST.get("precio_contacto")
        #redactar correo
        ctx = {
            'nombre': nombre,
            'apellido': apellido,
            'telefono': telefono,
            'correo': email,
            'texto': texto,
            'titulo':titulo,
            'precio':precio
        }
        #info correo
        subject = "Segundas"
        to = [correocontacto]
        from_email = 'acervantesbsv@gmail.com'
        message = render_to_string('segundasapp/email-contacto.html',ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        c = Contacto.objects.create(titulo=titulo,fecha=datetime.datetime.now())
        #procesar imagenes enviadas
        if(len(request.FILES.getlist('imagenes_contacto'))!= 0):
            files = request.FILES.getlist('imagenes_contacto')

            for fileach in files:
                a = Attachment.objects.create(fileattach=fileach,fichacontacto=c)
                print(a.fileattach.name)
                msg.attach_file(a.fileattach.name)
        else:
            print("no se subieron archivos")

        if(msg.send()):
            c.delete()
            print("sent")
        else:
            print("no se envio:")
            print(c.pk)

        # redirect to a new URL:
        print("Valido!")
        return HttpResponseRedirect('/')
    # else:
    #     print("No valida!")
    #     return HttpResponseRedirect('/contacto/')
    # if a GET (or any other method) we'll create a blank form
    #else:
    #    form = ContactoMail()
    return HttpResponseRedirect('/contacto/')

def venta_segundas(request):
    segundas = Segunda.objects.filter().order_by('titulo')
    cuenta = math.ceil(segundas.count()/lim)
    segundas = segundas[:lim]
    form = BusquedaSegundas(request.POST)
    formseg = ContactoSegunda(request.POST)
    return render(request,'segundasapp/venta_segundas.html',{'form':form,'formseg':formseg,\
                  'segundas':segundas,'count':range(cuenta)})

def ordenar_segundas(request):
    if request.method == 'GET':
        body = json.loads(request.GET.get('formData'))
        ordenar = request.GET.get('ordenar')
        asc = request.GET.get('asc')
        print(asc)
        formdict = {}
        for i in body:
            formdict[i['name']]= i['value']
        if(formdict['categoria_busqueda']=='T'):
            segundas = Segunda.objects.filter(Q(titulo__icontains=formdict['titulo_busqueda']) \
            | Q(titulo__icontains=formdict['titulo_busqueda'])).order_by('titulo')
            #print(float(formdict['precio_busqueda_max']))
            if(float(formdict['precio_busqueda_max'])!=0):
                segundas = segundas.filter(precio__gte=formdict['precio_busqueda_min'],\
                precio__lte=formdict['precio_busqueda_max']).order_by('titulo')
        else:
            segundas = Segunda.objects.filter(Q(titulo__icontains=formdict['titulo_busqueda']) \
            | Q(contenido__icontains=formdict['titulo_busqueda']), \
            Q(categoria__exact=formdict['categoria_busqueda'])).order_by('titulo')
            if(float(formdict['precio_busqueda_max'])!=0):
                segundas = segundas.filter(precio__gte=formdict['precio_busqueda_min'],\
                precio__lte=formdict['precio_busqueda_max']).order_by('titulo')
        if(ordenar=='fecha'):
            if(int(asc)==1):
                segundas = segundas.order_by('-fecha')
            else:
                if(int(asc)==0):
                    segundas = segundas.order_by('fecha')
        else:
            if(int(asc)==1):
                segundas = segundas.order_by('-precio')
            else:
                if(int(asc)==0):
                    segundas = segundas.order_by('precio')
        cuenta = math.ceil(segundas.count()/lim)
        segundas = segundas[:lim]
        formseg = ContactoSegunda(request.POST)
        csrf_token_value = get_token(request);
        paginate = render_to_string('segundasapp/paginate.html',{'count':range(cuenta)})
        segundas = render_to_string('segundasapp/filtro_segundas.html',\
        {'segundas':segundas,'formseg':formseg,'token':csrf_token_value})
        jsonData = {}
        jsonData['segundas'] = segundas
        jsonData['paginate'] = paginate
        jsonData = json.dumps(jsonData)
        return HttpResponse(jsonData)
    else:
        return HttpResponse("error")

def filtro_segundas(request):
    if request.method == 'GET':
        body = json.loads(request.GET.get('formData'))
        formdict = {}
        for i in body:
            formdict[i['name']]= i['value']
            print(i['name'])
            print(i['value'])

        if(formdict['categoria_busqueda']=='T'):
            segundas = Segunda.objects.filter(Q(titulo__icontains=formdict['titulo_busqueda']) \
            | Q(titulo__icontains=formdict['titulo_busqueda'])).order_by('titulo')
            #print(float(formdict['precio_busqueda_max']))
            if(float(formdict['precio_busqueda_max'])!=0):
                segundas = segundas.filter(precio__gte=formdict['precio_busqueda_min'],\
                precio__lte=formdict['precio_busqueda_max']).order_by('titulo')
        else:
            segundas = Segunda.objects.filter(Q(titulo__icontains=formdict['titulo_busqueda']) \
            | Q(contenido__icontains=formdict['titulo_busqueda']), \
            Q(categoria__exact=formdict['categoria_busqueda'])).order_by('titulo')
            if(float(formdict['precio_busqueda_max'])!=0):
                segundas = segundas.filter(precio__gte=formdict['precio_busqueda_min'],\
                precio__lte=formdict['precio_busqueda_max']).order_by('titulo')
        cuenta = math.ceil(segundas.count()/lim)
        segundas = segundas[:lim]
        formseg = ContactoSegunda(request.POST)
        csrf_token_value = get_token(request);
        paginate = render_to_string('segundasapp/paginate.html',{'count':range(cuenta)})
        segundas = render_to_string('segundasapp/filtro_segundas.html',\
        {'segundas':segundas,'formseg':formseg,'token':csrf_token_value})
        jsonData = {}
        jsonData['segundas'] = segundas
        jsonData['paginate'] = paginate
        jsonData = json.dumps(jsonData)
        return HttpResponse(jsonData)
    else:
        return HttpResponse('request inválido')
    return HttpResponse('Hubo un problema')

def page_segundas(request):
    if request.method == 'GET':
        body = json.loads(request.GET.get('formData'))
        page = int((request.GET.get('page')))
        filtro = request.GET.get('ordenar')
        asc = request.GET.get('asc')
        print(page)
        print(asc)
        formdict = {}
        for i in body:
            formdict[i['name']]= i['value']
            print(i['name'])
            print(i['value'])
        if(formdict['categoria_busqueda']=='T'):
            segundas = Segunda.objects.filter(Q(titulo__icontains=formdict['titulo_busqueda']) \
            | Q(titulo__icontains=formdict['titulo_busqueda'])).order_by('titulo')
            #print(float(formdict['precio_busqueda_max']))
            if(float(formdict['precio_busqueda_max'])!=0):
                segundas = segundas.filter(precio__gte=formdict['precio_busqueda_min'],\
                precio__lte=formdict['precio_busqueda_max']).order_by('titulo')
        else:
            segundas = Segunda.objects.filter(Q(titulo__icontains=formdict['titulo_busqueda']) \
            | Q(contenido__icontains=formdict['titulo_busqueda']), \
            Q(categoria__exact=formdict['categoria_busqueda'])).order_by('titulo')
            if(float(formdict['precio_busqueda_max'])!=0):
                segundas = segundas.filter(precio__gte=formdict['precio_busqueda_min'],\
                precio__lte=formdict['precio_busqueda_max']).order_by('titulo')
        if(filtro=='fecha'):
            if(int(asc)==1):
                segundas = segundas.order_by('-fecha')
            else:
                if(int(asc)==0):
                    segundas = segundas.order_by('fecha')
        if(filtro=='precio'):
            if(int(asc)==1):
                segundas = segundas.order_by('-precio')
            else:
                if(int(asc)==0):
                    segundas = segundas.order_by('precio')
        liminf = (page-1)*lim
        limsup = page*lim
        print(liminf)
        print(limsup)
        segundas = segundas[liminf:limsup]
        formseg = ContactoSegunda(request.POST)
        csrf_token_value = get_token(request);
        segundas = render_to_string('segundasapp/filtro_segundas.html',\
        {'segundas':segundas,'formseg':formseg,'token':csrf_token_value})
        jsonData = {}
        jsonData['segundas'] = segundas
        jsonData['pageactive'] = page
        jsonData = json.dumps(jsonData)
        return HttpResponse(jsonData)
    else:
        return HttpResponse('request inválido')
    return HttpResponse('Hubo un problema')
