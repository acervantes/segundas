#Imports.
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?:contacto)/$', views.contacto, name='contacto'),
    url(r'^(?:get_mensaje_contacto)/$', views.get_mensaje_contacto, name='get_mensaje_contacto'),
    url(r'^(?:venta_segundas)/$', views.venta_segundas, name='venta_segundas'),
    url(r'^(?:filtro_segundas)/$', views.filtro_segundas, name='filtro_segundas'),
    url(r'^(?:ordenar_segundas)/$', views.ordenar_segundas, name='ordenar_segundas'),
    url(r'^(?:page_segundas)/$', views.page_segundas, name='page_segundas'),
    url(r'^(?:google5a135354c3e98127)/$', views.google5a135354c3e98127, name='google5a135354c3e98127'),
    # url(r'^(?:prueba)/$', views.prueba, name='prueba'),
    url(r'^(?:get_contacto_segunda)/$', views.get_contacto_segunda, name='get_contacto_segunda'),
]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
