from django.contrib import admin
from django.shortcuts import get_object_or_404
from multiupload.admin import MultiUploadAdmin
# Register your models here.
from .models import Segunda, Image, Contacto, Attachment

class ImageInlineAdmin(admin.TabularInline):
    model = Image

class SegundaAdmin(MultiUploadAdmin):
    # default value of all parameters:
    inlines = [
            ImageInlineAdmin,
        ]
    change_form_template = 'multiupload/change_form.html'
    change_list_template = 'multiupload/change_list.html'
    multiupload_template = 'multiupload/upload.html'
    # if true, enable multiupload on list screen
    # generaly used when the model is the uploaded element
    multiupload_list = True
    # if true enable multiupload on edit screen
    # generaly used when the model is a container for uploaded files
    # eg: gallery
    # can upload files direct inside a gallery.
    multiupload_form = True
    # max allowed filesize for uploads in bytes
    multiupload_maxfilesize = 3 * 2 ** 20 # 3 Mb
    # min allowed filesize for uploads in bytes
    multiupload_minfilesize = 0
    # tuple with mimetype accepted
    multiupload_acceptedformats = ( "image/jpeg",
                                    "image/pjpeg",
                                    "image/png",)

    def process_uploaded_file(self, uploaded, object, request):
        '''
        Process uploaded file
        Parameters:
            uploaded: File that was uploaded
            object: parent object where multiupload is
            request: request Object
        Must return a dict with:
        return {
            'url': 'url to download the file',
            'thumbnail_url': 'some url for an image_thumbnail or icon',
            'id': 'id of instance created in this method',
            'name': 'the name of created file',

            # optionals
            "size": "filesize",
            "type": "file content type",
            "delete_type": "POST",
            "error" = 'Error message or jQueryFileUpload Error code'
        }
        '''
        # example: kwargs.get('title', [''])[0] or
        if object:
            image = object.images.create(fileimage=uploaded)
        else:
            image = Image.objects.create(fileimage=uploaded, gallery=None)
        return {
            'url': image.fileimage.url,
            'thumbnail_url': image.fileimage.url,
            'id': image.id,
            'name': image.filename
        }

    def delete_file(self, pk, request):
        '''
        Function to delete a file.
        '''
        # This is the default implementation.
        obj = get_object_or_404(self.queryset(request), pk=pk)
        obj.delete()


admin.site.register(Segunda,SegundaAdmin)
admin.site.register(Contacto)
admin.site.register(Attachment)
#admin.site.register(Image,ImageAdmin)
