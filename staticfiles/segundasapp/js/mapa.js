/* Google Map inside of tab */
function initTabMap() {
  // Google Map Options
  var mapOptions1 = {
    // How zoomed in you want the map to start at (always required)
    zoom: 15,

    // The latitude and longitude to center the map (always required)
    center: new google.maps.LatLng(19.427040, -99.214437), // Providence, RI

    // Map Styling
    styles: [{
      "featureType": "administrative.country",
      "elementType": "geometry",
      "stylers": [{
        "visibility": "simplified"
      }, {
        "hue": "#ff0000"
      }]
    }]
  };
  // Get the HTML DOM element that will contain your map
  //  div id="map_2"
  var mapElement1 = document.getElementById('map');

  // Create the Google Map using our element and options defined above
  var map1 = new google.maps.Map(mapElement1, mapOptions1); /* Tab Map */

  /* Providence Marker */
  var marker1 = new google.maps.Marker({
    position: new google.maps.LatLng(19.427040, -99.214437),
    map: map1,
    title: 'BSV'
  });
}
