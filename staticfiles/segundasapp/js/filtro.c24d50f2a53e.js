function ajaxconsegunda(thisObj){
  formData = thisObj.serializeArray();
  formData = JSON.stringify(formData);
  tokencsrf = thisObj.children("input[name='csrfmiddlewaretoken']").val()
  $.ajaxSetup({
    headers: { "X-CSRFToken": tokencsrf }
  });
  $.ajax({
    method: 'POST',
    data: {'formData':formData},
    url: '/segundasapp/get_contacto_segunda/',
    success: function(newData) {
      $('.modal').modal('close');
      var $toastContent = $('<span>Se han recibido tus datos!<br>En breve te\
       contactaremos...</span>');
       Materialize.toast($toastContent, 5000);
       return true;
    },
    error: function(newData) {
      var $toastContent = $('<span>Error en envio de solictud...<br>\
      Verificar datos</span>');
      Materialize.toast($toastContent, 5000);
      return false;
    }
  });
}

$( document ).ready(function() {
  $('.modal').modal();
  $('select').material_select();
  $('form').submit(function(e) {
    e.preventDefault();
    if($(this).attr("id")=='filtross'){
      minimo = parseFloat($(this).find("input[name='precio_busqueda_min']").val());
      if( minimo !== minimo){
        minimo = 0;
        $(this).find("input[name='precio_busqueda_min']").val(0)
      }
      maximo = parseFloat($(this).find("input[name='precio_busqueda_max']").val())
      if(maximo !== maximo){
        maximo = 0;
        $(this).find("input[name='precio_busqueda_max']").val(0)
      }
      if( minimo > maximo){
        alert("el precio mínimo debe ser menor al máximo!!")
          return false;
       }
       else{
         formData = $(this).serializeArray();
         formData = JSON.stringify(formData);

         $.ajax({
           method: 'GET',
           data: {'formData':formData},
           url: '/segundasapp/filtro_segundas/',
           success: function(newData) {
             $("#segundas").html(newData);
             $('.modal').modal();
             e.preventDefault();
             $( document ).ready(function() {
               $("form[name='contactosegunda']").submit(function(e) {
                 e.preventDefault();
                 ajaxconsegunda($(this));
               });
             });
           },
           error: function() {
             alert("hubo un error");
           }
         });
         return true;
       }
      }
  else{
      ajaxconsegunda($(this));
    }
  });
});
